<?php

/*
 * [INTERNAL] Retorna o hash de configuracao da query passada como parametro
 *            (ou a configuracao de todas as queries, caso o parametro seja
 *            nulo)
 */
function ldap_active_text_get_query($query_name = '') {
	$query_config = variable_get('ldap_active_text_config', array());
	$result = NULL;

	if($query_name == '')
		// Retorna a configuracao de todas as queries
		$result = $query_config;

	if(isset($query_config[$query_name]))
		$result = $query_config[$query_name];

	return $result;
}


/*
 * [INTERNAL] Intepreta o corpo do texto ativo, substituindo
 *            os parametros pelos respectivos valores
 */
function ldap_active_text_parse($query_name, $keyword = '_DEFAULT_') {
	$q = ldap_active_text_get_query($query_name);
	$multiple = $q['multiple_values'];

	if($q['fixed_input'])
		// Query estatica (com parametro fixo): intepreta tokens caso o modulo esteja ativo
		$keyword = module_exists('token') ? token_replace($q['input_parameter']) : $q['input_parameter'];

	$data_set = ldap_active_text_evaluate_query($keyword, $query_name, $multiple);

	if(empty($data_set))
		return $q['display_prefix'] . $q['display_empty'] . $q['display_suffix'];

	$text = '';
	foreach($data_set as $data) {
		$line = $q['display_body'];
		$empty_line = TRUE;
		$static_body = TRUE; // Corpo estatico = nao tem nenhum parametro de substituicao

		if($q['conditional_display'])
			// Query com exibicao de resultados condicional
			if(! ldap_active_text_evaluate_display_condition($query_name, $data)) {
				$line = $q['conditional_false_result'];
				$empty_line = FALSE;
			}

		while(preg_match('/\{([a-z0-9_\-\.:]+)\}/i', $line, $matches, PREG_OFFSET_CAPTURE)) {
			$static_body = FALSE; // Encontrado pelo menos 1 token
			$offset = $matches[0][1];
			$length = strlen($matches[0][0]);
			$token = strtolower($matches[1][0]); // Token contem nomes de parametros e de active texts: deve ser minusculo
			$replace_str = '';
			switch(true) {
				case strpos($token, '.') !== FALSE: // Parametro de formatacao
					$parameters = explode('.', $token);
					if(isset($data[$parameters[0]]))
						switch($parameters[1]) {
							case 'jpg': $replace_str = 'data:image/jpeg;base64,' . base64_encode($data[$parameters[0]]); break;
						}
				break;
				case strpos($token, ':') !== FALSE: // Sub-queries
					$parameters = explode(':', $token);
					if(isset($data[$parameters[0]]))
						$replace_str = ldap_active_text_parse($parameters[1], $data[$parameters[0]]);
				break;
				case isset($data[$token]):
					$replace_str = $data[$token];
			}
			$line = substr_replace($line, $replace_str, $offset, $length);
			$empty_line = $empty_line && empty($replace_str);
		}
		// Se a linha tiver pelo menos 1 parametro e todos os parametros
		// da linha forem nulos, usa $q['display_empty']
		$text .= ($empty_line && ! $static_body) ? $q['display_empty'] : $line;
	}

	return $q['display_prefix'] . $text . $q['display_suffix'];
}


/*
 * [INTERNAL] Verifica as condicoes de visualizacao dos resultados da query
 *            Retorna TRUE se os valores devem ser exibidos
 */
function ldap_active_text_evaluate_display_condition($query_name, $data) {
	$query_config = ldap_active_text_get_query($query_name);
	$conditional_expressions = $query_config['conditional_expressions'];
	$conditional_type = $query_config['conditional_type'];

	$null = FALSE;
	foreach($conditional_expressions as $conditional_expression) {
		$parameters = explode(':', $conditional_expression);
		if(isset($parameters[1]))
			// Expressao na forma "nome_do_atributo:nome_da_query"
			$result = ldap_active_text_evaluate_query($data[$parameters[0]], $parameters[1]);
		else
			// Expressao na forma "nome_do_atributo"
			if(isset($data[$parameters[0]]))
				$result = $data[$parameters[0]];

		if($null = empty($result))
			break;
	}

	return $conditional_type == 'inverse' ? $null : ! $null;
}


/*
 * [INTERNAL] Retorna o valor da query $query_name para o atributo de entrada $keyword
 */
function ldap_active_text_evaluate_query($keyword, $query_name, $multiple = FALSE) {
	$query_config = ldap_active_text_get_query($query_name);
	$query_string = $query_config['query_string'];
	$cache_time   = $query_config['cache_time'];
	$result = NULL;

	if(empty($query_string))
		// Query nao existe
		return NULL;

	$cid = 'query:' . $query_name . ':' . $keyword;
	if($cache = cache_get($cid, 'cache_ldap_active_text')) {
		$data = $cache->data;

		if(isset($data))
			// Resultado encontrado no cache
			return $data;
	}

	if(preg_match_all('/\{([a-z0-9_\-]+:[a-z0-9_\-]+:[a-z0-9_\-,]+)\}/i', $query_string, $matches)) {
		$queries = $matches[1];
		$last_query_index = count($queries) - 1;
		for($i = 0; $i <= $last_query_index; $i++) {
			$query_parameters = explode(':', $queries[$i]);
			$output_attributes = explode(',', $query_parameters[2]);
			if($i == $last_query_index) {
				// Apenas a ultima query pode retornar multiplos valores para o(s) atributo(s) de saida
				$sorting_attribute = strtolower($output_attributes[0]);
				$result = ldap_active_text_ldap_query($keyword, $query_parameters[0], $query_parameters[1], $output_attributes, $multiple);
				if(empty($result))
					return NULL;
			}else {
				$result = ldap_active_text_ldap_query($keyword, $query_parameters[0], $query_parameters[1], $output_attributes);
				if(empty($result))
					return NULL;
				// As queries intermediarias nao suportam multiplos atributos de saida: observar apenas $result[0]
				if(empty($result[0][strtolower($query_parameters[2])]))
					return NULL;
				// Resultado da query intermediaria eh a keyword da proxima query
				$keyword =  $result[0][strtolower($query_parameters[2])];
			}
		}
	}

	if($multiple)
		// Ordena os resultados
		usort($result, function($a, $b) use ($sorting_attribute) {
			return strnatcmp($a[$sorting_attribute], $b[$sorting_attribute]);
		});

	// Guarda o resultado no cache
	cache_set($cid, $result, 'cache_ldap_active_text', REQUEST_TIME + 60 * $cache_time);

	return $result;
}


/*
 * [INTERNAL] Executa a query LDAP "$ldap_query_name" com o filtro "($keyword=$input_parameter)"
 *            e retorna os valores dos atributos especificados no array "$output_attributes"
 */
function ldap_active_text_ldap_query($keyword, $input_parameter, $ldap_query_name, $output_attributes, $multiple = FALSE) {
	$filter = strtolower("($input_parameter=$keyword)");
	$query_results = '';

	if($query = ldap_query_get_queries($ldap_query_name, 'enabled', TRUE, TRUE)) {
		if(strtolower($input_parameter) == 'dn') {
			// Busca por DN: buscar pela base e nao pelo filtro
			$query->baseDn = array($keyword);
			$query->scope = LDAP_SCOPE_BASE;
		} else
			$query->filter = '(&' . $query->filter . $filter . ')';
		$query->sizelimit = $multiple ? 0 : 1;
		$query_results = $query->query();
	}

	if(empty($query_results) || ! $query_results['count'])
		return NULL;

	$results = array();

	// Caso especial: atributos multivalorados
	// (so serao processados se a query soh retornar 1 atributo)
	// [TODO]: implementar suporte decente a atributos multivalorados
	if($multiple && ($query_results['count'] == 1) && count($output_attributes) == 1) {
		$values = $query_results[0][$output_attributes[0]];
		for($i = 0; $i < $values['count']; $i++) {
			$results[$i][$output_attributes[0]] = $values[$i];
			$results[$i]['dn'] = $query_results[0]['dn'];
		}
	return $results;
	}

	$result_count = $multiple ? $query_results['count'] : 1;

	// Retorna um array com os atributos, na forma atributo => valor
	for($i = 0; $i < $result_count; $i++) {
		$result = array();
		foreach($output_attributes as $output_attribute) {
			$output_attribute = strtolower($output_attribute);
			if(isset($query_results[$i][$output_attribute][0])) {
				$result[$output_attribute] = $query_results[$i][$output_attribute][0];
			}
		}
		$result['dn'] = $query_results[$i]['dn'];
		$results[] = $result;
	}

	return $results;
}
